
export const createGame = () =>
  fetch('/api/games', { method: 'post' })
    .then(response => response.headers.get('location'))

export const guessAnswer = (url, data) =>
  fetch(url, {
    method: 'PATCH',
    body: JSON.stringify(data),
    headers: { 'Content-Type': 'application/json' }
  })
    .then(response => response.json())

export const getAnswer = (url) =>
  fetch(url)
    .then(response => response.json())
